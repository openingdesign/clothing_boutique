#### DEMOLITION NOTES:
- SALVAGE THE FOLLOWING FOR POSSIBLE REUSE AND RELOCATION   
  - ACOUSTICAL TILE AND GRID   
  - BASE TRIM   
  - DOORS AND SIDELIGHTS  
  - LIGHT FIXTURES
- GC TO PROTECT ALL EXISTING CONSTRUCTION FROM DAMAGE AND DUST/FUMES AND PATCHED/REPAIRED AS NECESSARY
- SEAL THE INTAKE/EXHAUST OF ANY DUCTS THAT ROUTE INTO ADJOINING SPACES THAT ARE OUTSIDE THE DEMOLITION SCOPE. 
- GC TO COORDINATE THE DISCONNECTION AND CAPPING (AND ANY ASSOCIATED FEES) OF ANY SERVICE UTILITIES ASSOCIATED WITH THE DEMOLITION SCOPE
- ALL FLOORS TO BE LEVELED AS NECESSARY (GRINDING, PATCHING, LEVELING, CHISELING) TO RECEIVE ANY SCHEDULE FINISHED
- GC TO NOTIFY OWNER AND/OR LANDLORD OF ANY DEMOLITION THAT IS NECESSARY OUTSIDE TENANT SPACE
- ADDITIONAL DEMOLITION, BEYOND WHAT IS IDENTIFIED HERE, MAY BE REQUIRED TO ACCOMMODATE THE PLANNED CONSTRUCTION SCOPE.
- OWNER/GC/SUBS ARE RESPONSIBLE TO TEST IF REMOVED ITEMS CONTAIN ANY HAZARDOUS MATERIALS AND IF SO, ARE LEGALLY REMOVED IN COMPLIANCE WITH LOCAL/STATE/FEDERAL LAWS
- GC RESPONSIBLE FOR ANY TEMPORARY SHORING NECESSARY WHEN EXISTING STRUCTURAL MEMBERS ARE REMOVED TO ACCOMMODATE ANY NEW CONSTRUCTION.  ALL SHORING TO BE DESIGNED BY A LICENSED STRUCTURAL ENGINEER RETAINED BY THE GC
- SEE MECHANICAL/ELECTRICAL/PLUMBING/FIRE PROTECTION DRAWINGS FOR ANY ADDITIONAL DEMOLITION SCOPE
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc2MDY5NDIwXX0=
-->